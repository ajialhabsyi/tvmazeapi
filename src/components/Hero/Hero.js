import React from 'react';
import { Card, CardImg, Col, Form, Row, Button, ButtonGroup } from 'react-bootstrap';
import styled from 'styled-components';

const HeroImage = styled(CardImg)`
    object-fit: cover;
    object-position: 50% 25%;
    height: 550px;
    width: auto;
`

const HeroCard = styled(Card)`
    text-align: left;
    margin-top: 16px;
    border: 0px;
`

const ColSearch = styled(Col)`
    text-align: right;
    float: right;
`
const CardTitle = styled(Card.Title)`
    font-weight: bold;
`

const TextUsername = styled(Card.Text)`
    font-weight:500;
`

const Hero = ({ item, handleChange, getItems, name }) => {

    if (item.show.image != null) {
        return (
            <HeroCard className="bg-dark text-white" key={item.show.id}>
                <HeroImage src={item.show.image.original} alt="Card image" />
                <Card.ImgOverlay>
                    <Row>
                        <Col>
                            <CardTitle>Camflix</CardTitle>
                        </Col>
                        <ColSearch>
                            <Row>
                                <Col md={6}>
                                    <Form.Group controlId="formBasicSearch">
                                        <Form.Control
                                            type="text"
                                            placeholder="Input Movie Title Here..."
                                            value={name}
                                            onChange={handleChange}
                                        />
                                    </Form.Group>

                                </Col>
                                <Col md={2}>
                                    <ButtonGroup aria-label="Basic example">
                                        <Button variant="primary" disabled={!name} onClick={() => getItems(name)}>Search</Button>
                                        {/* <Button variant="secondary" onClick={() => getItems('girls')}>Reset</Button> */}
                                    </ButtonGroup>
                                </Col>
                                <Col md={4}>
                                    <TextUsername>Aji Ridwan Alhabsyi</TextUsername>
                                </Col>
                            </Row>
                        </ColSearch>
                    </Row>
                </Card.ImgOverlay>
            </HeroCard>
        )
    }
}

export default Hero