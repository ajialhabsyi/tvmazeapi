import { Card } from "react-bootstrap";
import styled from "styled-components";

const SwipeDateSection = styled.section`
    margin: auto; 
    text-align: center; 
    position: relative;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
    margin-bottom: 20px !important;
    width: 100%;
    padding-top: 5px;
`

const FlexContainer = styled.div`
    overflow-x: scroll;
    &::-webkit-scrollbar {
        display: none;
    }
    overflow-y: hidden;
    height: 100%;
    white-space: nowrap;
`

const FlexItem = styled.div`
    margin: 0px 16px;
    max-height: 500px;
    max-width: 400px;
    cursor: pointer;
    display:inline-block;
    vertical-align:top;
`

const CardList = styled(Card)`
    border: 0px;
`

const CardLink = styled.a`
    text-decoration: none;
    color: #6c757d;
`

const ImgCard = styled.img`
    object-fit: cover;
    width: 250px; 
    height: 350px;
`

const TitleCard = styled.p`
    white-space: normal !important;
    word-wrap: break-word !important;
`

const List = ({ items }) => {
    return (
        <SwipeDateSection className="mt-5 mb-5">
            <FlexContainer>
                {items.map((item, index) => {
                    if (item.show.image != null) {
                        return (
                            <FlexItem key={index}>
                                <CardLink href={item.show.url} target="_blank">
                                    <CardList>
                                        <ImgCard variant="top" src={item.show.image.medium} />
                                        <Card.Body>
                                            <TitleCard>{item.show.name}</TitleCard>
                                        </Card.Body>
                                    </CardList>
                                </CardLink>
                            </FlexItem>
                        )
                    }
                    else {
                        return (
                            <FlexItem key={index}>
                                <CardLink href={item.show.url} target="_blank">
                                    <CardList>
                                        <Card.Img variant="top" src="https://static.tvmaze.com/images/no-img/no-img-portrait-text.png" />
                                        <Card.Body>
                                            <TitleCard>{item.show.name}</TitleCard>
                                        </Card.Body>
                                    </CardList>
                                </CardLink>
                            </FlexItem>
                        )
                    }
                })}
            </FlexContainer>
        </SwipeDateSection>
    )
}

export default List