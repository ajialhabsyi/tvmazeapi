import { useEffect, useState } from 'react';
import axios from "axios";
import { Container } from "react-bootstrap"
import Hero from "../../components/Hero"
import List from "../../components/List"

const Home = () => {

    const [items, setItems] = useState([]);

    const [name, setName] = useState('');

    useEffect(() => {
        getItems('girls');
    }, []);

    const getItems = (keyword) => {

        axios({
            method: 'GET',
            url: 'http://api.tvmaze.com/search/shows?',
            params: {
                q: keyword
            }
        })
            .then((res) => {
                setItems(res.data);
            })
            .catch((error) => {
                console.log(error);
            })
    }

    const handleChange = (e) => {
        e.preventDefault();
        setName(e.target.value.toLowerCase())
    }

    return (
        <Container className="themed-container" fluid={true}>
            {items && items.length > 0 &&
                <Hero 
                    item={items[0]} 
                    handleChange={handleChange} 
                    getItems={getItems} 
                    name={name} 
                />
            }
            <List items={items} />
        </Container>
    )
}

export default Home