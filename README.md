# Junior Front-End Developer (Remote)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) and using https://www.tvmaze.com/api.

## Getting Started

```
git clone https://ajialhabsyi@bitbucket.org/ajialhabsyi/tvmazeapi.git
cd tvmazeapi
npm install
npm start # open localhost:3000
```